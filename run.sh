#!/bin/bash
set -e
set -o pipefail

service_name=sprofiler
service=false
prod=false
update=false
while getopts spu flag
do
    case "${flag}" in
        s) service=true;;
        p) prod=true;;
        u) update=true;;
    esac
done

if [ "$service" = true ] ; then
    sudo chmod +x server/src/server.js
    if systemctl is-active --quiet $service_name ; then
        sudo systemctl stop $service_name
    fi
fi

if [ "$update" = true ] ; then
    git pull
    npm install
    cd client
    npm install
    npm run build
    cd ..
fi

if [ "$service" = true ] ; then
    sudo systemctl start $service_name
else
    if [ "$prod" = true ] ; then
        PORT=8080 NODE_ENV=production npm start
    else
        PORT=8080 npm start
    fi
fi
