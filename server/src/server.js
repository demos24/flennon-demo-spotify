const { port } = require('./config');
var path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

const api = require('./api/api');
app.use('/api', api)

// Define Static Folder
app.use(express.static(path.resolve('server/public/')));

//Send static HTML
app.get(/.*/, (req, res) => res.sendFile(path.resolve('server/public/index.html')));

app.listen(port, () => console.log(`Server started on port ${port}`));