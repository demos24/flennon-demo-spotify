module.exports = {
    port: process.env.PORT,

    spotifyId: process.env.SPOTIFY_CLIENTID,
    spotifySecret: process.env.SPOTIFY_SECRET
}