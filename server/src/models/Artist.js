const { convertMsToMinutes } = require('../services/helperService');

class Artist {
    constructor({ profile, tracks, albums, relatedArtists }) {
        this.id = profile.id
        this.name = profile.name;
        this.profile = profile || {};
        this.tracks = tracks || [];
        this.albums = albums || [];
        this.relatedArtists = relatedArtists || [];
    }

    parseImages(images) {
        if (!images) return
        const sm = images.find(i => i.height < 200 && i.width < 200)?.url;
        const md = images.find(i => i.height > 200 && i.width > 200 && i && i.height < 600 && i.width < 600)?.url;
        const lg = images.find(i => i.height > 600 && i.width > 600)?.url;
        return { sm, md, lg }
    }

    parseTracks() {
        return this.tracks.map(track => {
            return {
                title: track.name,
                time: convertMsToMinutes(track.duration_ms),
                preview: track.preview_url,
                link: track.external_urls.spotify,
                images: this.parseImages(track.album.images)
            };
        })
    }

    ArtistDto() {
        return {
            id: this.id,
            name: this.name,
            link: this.profile.external_urls.spotify,
            images: this.parseImages(this.profile.images),
            topTracks: this.parseTracks()
        }
    }

    SearchArtistDto() {
        return {
            id: this.id,
            name: this.name,
            link: this.profile.external_urls.spotify,
            images: this.parseImages(this.profile.images),
            genres: this.profile.genres
        }

    }
}

module.exports = Artist;