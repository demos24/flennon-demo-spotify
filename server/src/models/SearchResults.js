const Artist = require('./Artist')

class SearchResults {
    constructor({ artists }) {
        this.artists = artists.map(a => new Artist({ profile: a }));
    }

    SearchResultsArtistDto() {
        return this.artists.map(a => a.SearchArtistDto())
    }
}

module.exports = SearchResults