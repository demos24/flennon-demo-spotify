const ArtistDto = {
    id: '0TnOYISbd1XYRBk9myaseg',
    name: 'Pitbull',
    link: 'https://open.spotify.com/artist/0TnOYISbd1XYRBk9myaseg',
    images: {
        lg: 'https://i.scdn.co/image/1353990534aef10c946cf3a47865ac22471be5c4',
        md: 'https://i.scdn.co/image/f3a0aca9f85c6cdad37ebcc51a01879bf8c50af9',
        sm: 'https://i.scdn.co/image/5ede30313c461d7ed0f5c4f3dea484fd349d49a9',
    },
    topTracks: [
        {
            title: 'Give Me Everything (feat. Ne-Yo, Afrojack & Nayer)',
            time: '4:12',
            preview: 'https://p.scdn.co/mp3-preview/3906aa5012624d4e35c2108207745eb48f3bdf0f?cid=774b29d4f13844c495f206cafdad9c86',
            link: 'https://open.spotify.com/track/4QNpBfC0zvjKqPJcyqBy9W',
            images: {
                lg: 'https://i.scdn.co/image/ab67616d0000b273c85676e37cf146675a250054',
                md: 'https://i.scdn.co/image/ab67616d00001e02c85676e37cf146675a250054',
                sm: 'https://i.scdn.co/image/ab67616d00004851c85676e37cf146675a250054',
            }
        },
        {
            title: 'Timber (feat. Ke$ha)',
            time: '3:24',
            preview: 'https://p.scdn.co/mp3-preview/4c8551c63855c08ef787b03a7c75f720831b4812?cid=774b29d4f13844c495f206cafdad9c86',
            link: 'https://open.spotify.com/track/3tgZ9vmhuAY9wEoNUJskzV',
            images: {
                lg: 'https://i.scdn.co/image/ab67616d0000b2735c3714e2591b30f2f5194704',
                md: 'https://i.scdn.co/image/ab67616d00001e025c3714e2591b30f2f5194704',
                sm: 'https://i.scdn.co/image/ab67616d000048515c3714e2591b30f2f5194704',
            }
        },
        {
            title: 'Time of Our Lives',
            time: '3:49',
            preview: 'https://p.scdn.co/mp3-preview/456046f512b8da97b84928f67cf9d8b2594db484?cid=774b29d4f13844c495f206cafdad9c86',
            link: 'https://open.spotify.com/track/2bJvI42r8EF3wxjOuDav4r',
            images: {
                lg: 'https://i.scdn.co/image/ab67616d0000b2731e340d1480e7bb29a45e3bd7',
                md: 'https://i.scdn.co/image/ab67616d00001e021e340d1480e7bb29a45e3bd7',
                sm: 'https://i.scdn.co/image/ab67616d000048511e340d1480e7bb29a45e3bd7',
            }
        },
        {
            title: 'Feel This Moment (feat. Christina Aguilera)',
            time: '3:50',
            preview: 'https://p.scdn.co/mp3-preview/68f4c7804fff74deba04b861d13d2743101233d5?cid=774b29d4f13844c495f206cafdad9c86',
            link: 'https://open.spotify.com/track/1obZSxOoO12yWkwY1kY72N',
            images: {
                lg: 'https://i.scdn.co/image/ab67616d0000b2735c3714e2591b30f2f5194704',
                md: 'https://i.scdn.co/image/ab67616d00001e025c3714e2591b30f2f5194704',
                sm: 'https://i.scdn.co/image/ab67616d000048515c3714e2591b30f2f5194704',
            }
        },
        {
            title: 'International Love (feat. Chris Brown)',
            time: '3:47',
            preview: 'https://p.scdn.co/mp3-preview/23235746b5a67425fe870638ede360e7f4177f00?cid=774b29d4f13844c495f206cafdad9c86',
            link: 'https://open.spotify.com/track/62zFEHfAYl5kdHYOivj4BC',
            images: {
                lg: 'https://i.scdn.co/image/ab67616d0000b273c85676e37cf146675a250054',
                md: 'https://i.scdn.co/image/ab67616d00001e02c85676e37cf146675a250054',
                sm: 'https://i.scdn.co/image/ab67616d00004851c85676e37cf146675a250054',
            }
        },
    ]
}

const SearchArtistDto = {
    id: '0TnOYISbd1XYRBk9myaseg',
    name: 'Pitbull',
    link: 'https://open.spotify.com/artist/0TnOYISbd1XYRBk9myaseg',
    images: {
        lg: 'https://i.scdn.co/image/1353990534aef10c946cf3a47865ac22471be5c4',
        md: 'https://i.scdn.co/image/f3a0aca9f85c6cdad37ebcc51a01879bf8c50af9',
        sm: 'https://i.scdn.co/image/5ede30313c461d7ed0f5c4f3dea484fd349d49a9',
    },
    genres: [
        "dance pop",
        "latin",
        "miami hip hop",
        "pop",
        "pop dance",
        "pop rap"
    ],
}

module.exports = {
    ArtistDto,
    SearchArtistDto
}