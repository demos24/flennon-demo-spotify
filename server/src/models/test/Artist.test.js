const testArtistProfile = require('./testArtistProfile.json');
const testArtistTracks = require('./testArtistTracks.json');
const { ArtistDto, SearchArtistDto } = require('./testArtist')
const Artist = require('../Artist');

describe("Testing parsing of Artists", () => {
    const artist = new Artist({ profile: testArtistProfile, tracks: testArtistTracks.tracks});
    
    test("Testing Artist DTO parsing", () => {    
        expect(artist.ArtistDto()).toEqual(ArtistDto)
    })

    test("Testing Search Result Artist DTO parsing", () => {    
        expect(artist.SearchArtistDto()).toEqual(SearchArtistDto)
    })
})