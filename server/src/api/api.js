const express = require('express');
const router = express.Router();
const { getSearch, getArtist } = require('../services/libraryService')

router.get('/search', async (req, res) => {
    const search = await getSearch(req.query.q);
    res.send(search);
})

router.get('/artist/:id', async (req, res) => {
    const artist = await getArtist(req.params.id);
    res.send(artist);
})

module.exports = router