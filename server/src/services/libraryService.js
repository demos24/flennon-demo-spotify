const { getSpotifySearch, getSpotifyArtist } = require('./spotifyService');
const Artist = require('../models/Artist');
const SearchResults = require('../models/SearchResults');

const getSearch = async (q) => {
    const response = await getSpotifySearch(q);
    const searchResults = new SearchResults({ artists: response.artists.items });
    return searchResults.SearchResultsArtistDto();
}

const getArtist = async (id) => {
    const profile = await getSpotifyArtist(id, '');
    const { tracks } = await getSpotifyArtist(id, 'top-tracks');
    const artist = new Artist({ profile, tracks });
    return artist.ArtistDto();
}

const getArtistTracks = async (id) => {
    return await getSpotifyArtist(id, 'top-tracks');
}

module.exports = {
    getSearch,
    getArtist,
    getArtistTracks
}