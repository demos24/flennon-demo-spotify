const axios = require('axios');
const { default: base64url } = require('base64url');
const qs = require('qs');
const { spotifyId, spotifySecret } = require('../config');

const getSpotifySearch = async (q) => {
    const token = await getSpotifyToken();
    const request = {
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` },
        url: `https://api.spotify.com/v1/search?q=${q}&type=artist`
    }
    const response = await axios(request);
    return response.data;
}

const getSpotifyArtist = async (id, type) => {
    const token = await getSpotifyToken();
    const request = {
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` },
        url: getArtistsUrl(id, type)
    }
    const response = await axios(request);
    return response.data;
}

const getSpotifyToken = async () => {
    const authHeaderBase64 = base64url(`${spotifyId}:${spotifySecret}`);
;    const request = {
        method: 'POST',
        headers: { 
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${authHeaderBase64}`
        },
        data: qs.stringify({ grant_type: 'client_credentials' }),
        url: 'https://accounts.spotify.com/api/token'
    }
    const response = await axios(request);
    return response.data.access_token;
}

const getArtistsUrl = (id, type, offset = 0, limit = 10) => {
    const baseUrl = `https://api.spotify.com/v1/artists/${id}`;
    switch (type) {
        case 'top-tracks':
            return baseUrl + '/top-tracks?market=GB';
        case 'albums':
            return baseUrl + `/albums?market=GB&limit=${limit}&offset=${offset}`;
        case 'related-artists':
            return baseUrl + `/related-artists`;
        default: 
            return baseUrl;
    }
}

module.exports = {
    getSpotifySearch,
    getSpotifyArtist,
    getArtistsUrl
}