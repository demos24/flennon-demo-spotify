const { getArtistsUrl } = require('../spotifyService');

test("Testing constructing the Artist URL", () => {
    expect(getArtistsUrl('4gzpq5DPGxSnKTe4SA8HAU')).toBe('https://api.spotify.com/v1/artists/4gzpq5DPGxSnKTe4SA8HAU');
    expect(getArtistsUrl('0TnOYISbd1XYRBk9myaseg', 'albums', 10, 5)).toBe('https://api.spotify.com/v1/artists/0TnOYISbd1XYRBk9myaseg/albums?market=GB&limit=5&offset=10');
    expect(getArtistsUrl('4gzpq5DPGxSnKTe4SA8HAU', 'top-tracks')).toBe('https://api.spotify.com/v1/artists/4gzpq5DPGxSnKTe4SA8HAU/top-tracks?market=GB');
    expect(getArtistsUrl('0TnOYISbd1XYRBk9myaseg', 'related-artists')).toBe('https://api.spotify.com/v1/artists/0TnOYISbd1XYRBk9myaseg/related-artists');
})