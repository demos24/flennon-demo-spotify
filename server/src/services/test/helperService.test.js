const { convertMsToMinutes } = require('../helperService');

describe("Testing Converting Milliseconds returns correct value", () => {
    test("Testing multiples of 10", () => {
        expect(convertMsToMinutes(100)).toBe('0:00');
        expect(convertMsToMinutes(1000)).toBe('0:01');
        expect(convertMsToMinutes(10000)).toBe('0:10');
        expect(convertMsToMinutes(100000)).toBe('1:40');
        expect(convertMsToMinutes(1000000)).toBe('16:40');
    })

    test("Testing multiples edge of minute mark", () => {
        expect(convertMsToMinutes(179999)).toBe('3:00');
    })

    test("Testing arbitary time of 10", () => {
        expect(convertMsToMinutes(185300)).toBe('3:05');
    })
})


test("Testing Converting Milliseconds to Minutes and Seconds is correct format", () => {
    for (var i = 0; i < 1000000; i = i * 2 + 7) {
        expect(convertMsToMinutes(i)).toMatch(/^([0-9]?[0-9]):[0-5][0-9]$/);
    }
})

