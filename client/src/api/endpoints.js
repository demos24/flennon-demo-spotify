import { apiUrl } from '../constants';

export const searchUrl  = `${apiUrl}/api/search`;
export const artistUrl  = `${apiUrl}/api/artist`;
