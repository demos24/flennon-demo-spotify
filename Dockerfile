FROM node:16

WORKDIR /usr/src/app
COPY . .

RUN npm install
WORKDIR /usr/src/app/client
RUN npm install
RUN npm run build

WORKDIR /usr/src/app

ENV PORT 8080
EXPOSE 8080
CMD [ "node", "/usr/src/app/server/src/server.js" ]
